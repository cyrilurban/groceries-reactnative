import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native'

class Groceries extends Component {
    // obj for user's email and pass
    user = {
        email: '',
        password: ''
    }
    // for
    handleEmail = (text) => {
        this.setState({ email: text })
    }
    handlePassword = (text) => {
        this.setState({ password: text })
    }

    // login and alert groceries list
    login = (email, pass) => {
        return fetch('https://api.everlive.com/v1/GWfRtXi1Lwt4jcqK/oauth/token', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: email,
                password: pass,
                grant_type: "password"
            })
        })
        .then((response) => response.json())
        .then((data) => {
            return fetch('https://api.everlive.com/v1/GWfRtXi1Lwt4jcqK/Groceries', {
                headers: {
                    "Authorization": "Bearer " + data.Result.access_token
                }
            })
        })
        .then((response) => response.json())
        .then((data) => {
        var result = "";
            data.Result.forEach(function(grocery) {
            result = result + "\n" + grocery.Name;
        });
            alert("List: " + result);
        })
        .catch((error) => {
            console.error(error);
        });
    }

    // view
    render(){
        return (
            <View style = {styles.container}>
                <View style={styles.header} >
                    <Text style={styles.welcome}>
                        Sign in
                    </Text>
                </View>
                <Image source={require('./image/logo.png')} style = {styles.Image}/>

                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    keyboardType = "email-address"
                    placeholder = " Email Adress"
                    placeholderTextColor = 'grey'
                    autoCapitalize = "none"
                    onChangeText = {this.handleEmail}/>

                <TextInput style = {styles.input}
                    secureTextEntry = {true}
                    underlineColorAndroid = "transparent"
                    placeholder = " Password"
                    placeholderTextColor = 'grey'
                    autoCapitalize = "none"
                    onChangeText = {this.handlePassword}/>

                <TouchableOpacity
                    style = {styles.submitButton}
                    onPress = {
                        () => this.login(this.state.email, this.state.password)
                    }>
                    <Text style = {styles.submitButtonText}> SIGN IN </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style = {styles.singUpButton}
                    onPress = {
                        () => alert("Sign up function will continue later...")
                    }>
                    <Text style = {styles.submitButtonText}> SIGN UP FOR GROCERIES </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

// styles
const styles = StyleSheet.create({
    container: {
        paddingTop: 23
    },
    Image: {
        justifyContent: "center",
        alignItems:'center',
        marginLeft: 25,
        marginTop: 20,
        marginBottom: 80
    },
    input: {
        margin: 10,
        height: 40,
        borderColor: 'grey',
        borderBottomWidth: 1
    },
    submitButton: {
        backgroundColor: 'grey',
        padding: 10,
        margin: 10,
        height: 40
    },
    singUpButton: {
        backgroundColor: 'white',
        borderColor: 'grey',
        borderWidth: 1,
        padding: 10,
        margin: 10,
        height: 40
    },
    submitButtonText:{
        color: 'black',
        textAlign: 'center'
    },
        welcome: {
        fontSize: 20,
        textAlign: 'left',
        margin: 10,
        color: 'white'
    },
    header: {
        height: 50,
        backgroundColor: '#2E6DAD'
    }
})

export default Groceries
